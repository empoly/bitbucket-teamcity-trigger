package ut.nl.empoly.bitbucket.servlet;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import nl.empoly.bitbucket.message.I18nFallback;
import nl.empoly.bitbucket.servlet.ServerConfigurationServlet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ServerConfigurationServletTest {

    private I18nResolver i18n;
    private SoyTemplateRenderer renderer;
    private PluginSettingsFactory settingsFactory;
    private UserManager userManager;
    private LoginUriProvider loginUriProvider;

    private ServerConfigurationServlet servlet;

    private HttpServletRequest request;
    private HttpServletResponse response;

    @Before
    public void setup() {
        i18n = new I18nFallback();
        renderer = mock(SoyTemplateRenderer.class);
        settingsFactory = mock(PluginSettingsFactory.class);
        userManager = mock(UserManager.class);
        loginUriProvider = mock(LoginUriProvider.class);

        servlet = new ServerConfigurationServlet(renderer, i18n, settingsFactory, userManager, loginUriProvider);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAuthorization() {
        UserKey userKey = new UserKey("test");
        when(userManager.getRemoteUserKey(request)).thenReturn(userKey);
    }
}
