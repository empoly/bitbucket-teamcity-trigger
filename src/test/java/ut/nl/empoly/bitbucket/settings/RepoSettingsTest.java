package ut.nl.empoly.bitbucket.settings;

import com.atlassian.bitbucket.repository.MinimalRef;
import com.atlassian.bitbucket.setting.Settings;
import nl.empoly.bitbucket.settings.RepoSettings;
import nl.empoly.bitbucket.settings.SettingsErrors;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by roy on 01-11-16.
 */
public class RepoSettingsTest {
    private static final String KEY_BUILD_CONFIG_1 = String.format(RepoSettings.KEY_BUILD_CONFIG_FORMAT, 1);
    private static final String KEY_DEFAULT_BRANCH_1 = String.format(RepoSettings.KEY_DEFAULT_BRANCH_FORMAT, 1);
    private static final String KEY_ADDITIONAL_BRANCHES_FILTER_1 = String.format(RepoSettings.KEY_ADDITIONAL_BRANCHES_FILTER_FORMAT, 1);

    @Test
    public void testTriggerCountValidation() {
        final SettingsErrors errors = new SettingsErrors();
        final Settings settings = mock(Settings.class);
        when(settings.getString(Mockito.anyString(), Mockito.anyString())).thenReturn("");

        when(settings.getInt(eq(RepoSettings.KEY_CONFIG_COUNT), Mockito.anyInt())).thenReturn(0);
        new RepoSettings(settings, errors, null);
        assertTrue("Error should be present for 0 triggers", errors.containsKey(null));
        errors.clear();

        when(settings.getInt(eq(RepoSettings.KEY_CONFIG_COUNT), Mockito.anyInt())).thenReturn(1);
        new RepoSettings(settings, errors, null);
        assertFalse("Error should not be present for 1 triggers", errors.containsKey(null));
        errors.clear();

        when(settings.getInt(eq(RepoSettings.KEY_CONFIG_COUNT), Mockito.anyInt())).thenReturn(RepoSettings.MAX_TRIGGER_COUNT + 1);
        new RepoSettings(settings, errors, null);
        assertTrue("Error should be present for (MAX_TRIGGER_COUNT + 1) triggers", errors.containsKey(null));
        errors.clear();
    }

    @Test
    public void testBuildConfigValidation() {
        final SettingsErrors errors = new SettingsErrors();
        final Settings settings = mock(Settings.class);
        when(settings.getInt(eq(RepoSettings.KEY_CONFIG_COUNT), Mockito.anyInt())).thenReturn(1);
        when(settings.getString(Mockito.anyString(), Mockito.anyString())).thenReturn("");

        when(settings.getString(eq(KEY_BUILD_CONFIG_1), Mockito.anyString())).thenReturn(" ");
        new RepoSettings(settings, errors, null);
        assertTrue("Error should be present for blank build config", errors.containsKey(KEY_BUILD_CONFIG_1));
        errors.clear();

        when(settings.getString(eq(KEY_BUILD_CONFIG_1), Mockito.anyString())).thenReturn("buildConfig");
        new RepoSettings(settings, errors, null);
        assertFalse("Error should not be present when build config is present", errors.containsKey(KEY_BUILD_CONFIG_1));
        errors.clear();
    }

    @Test
    public void testDefaultBranchValidation() {
        final SettingsErrors errors = new SettingsErrors();
        final Settings settings = mock(Settings.class);
        when(settings.getInt(eq(RepoSettings.KEY_CONFIG_COUNT), Mockito.anyInt())).thenReturn(1);
        when(settings.getString(Mockito.anyString(), Mockito.anyString())).thenReturn("");

        when(settings.getString(eq(KEY_DEFAULT_BRANCH_1), Mockito.anyString())).thenReturn(" ");
        new RepoSettings(settings, errors, null);
        assertTrue("Error should be present for blank default branch", errors.containsKey(KEY_DEFAULT_BRANCH_1));
        errors.clear();

        when(settings.getString(eq(KEY_DEFAULT_BRANCH_1), Mockito.anyString())).thenReturn("/refs/heads/develop");
        new RepoSettings(settings, errors, null);
        assertFalse("Error should not be present when default branch is present", errors.containsKey(KEY_DEFAULT_BRANCH_1));
        errors.clear();
    }

    @Test
    public void testBranchFilterValidation() {
        final SettingsErrors errors = new SettingsErrors();
        final Settings settings = mock(Settings.class);
        when(settings.getInt(eq(RepoSettings.KEY_CONFIG_COUNT), Mockito.anyInt())).thenReturn(1);
        when(settings.getString(Mockito.anyString(), Mockito.anyString())).thenReturn("");

        when(settings.getString(eq(KEY_ADDITIONAL_BRANCHES_FILTER_1), Mockito.anyString())).thenReturn(" ");
        new RepoSettings(settings, errors, null);
        assertFalse("Error should not be present for blank branch filter", errors.containsKey(KEY_ADDITIONAL_BRANCHES_FILTER_1));
        errors.clear();

        when(settings.getString(eq(KEY_ADDITIONAL_BRANCHES_FILTER_1), Mockito.anyString())).thenReturn("(valid)[reg]ex");
        new RepoSettings(settings, errors, null);
        assertFalse("Error should not be present for valid branch filter", errors.containsKey(KEY_ADDITIONAL_BRANCHES_FILTER_1));
        errors.clear();

        when(settings.getString(eq(KEY_ADDITIONAL_BRANCHES_FILTER_1), Mockito.anyString())).thenReturn("invalid(regex");
        new RepoSettings(settings, errors, null);
        assertTrue("Error should be present for invalid branch filter", errors.containsKey(KEY_ADDITIONAL_BRANCHES_FILTER_1));
        errors.clear();
    }

    @Test
    public void testErrorMetaData() {
        final SettingsErrors errors = new SettingsErrors();
        final Settings settings = mock(Settings.class);
        when(settings.getInt(eq(RepoSettings.KEY_CONFIG_COUNT), Mockito.anyInt())).thenReturn(1);
        when(settings.getString(Mockito.anyString(), Mockito.anyString())).thenReturn("");

        new RepoSettings(settings, errors, null);
        assertTrue("Error should be present for empty build config", errors.containsKey(KEY_BUILD_CONFIG_1));
        assertTrue("Errors should contain metadata for trigger with errors", errors.containsKey(String.format(RepoSettings.KEY_META_TRIGGER_HAS_ERRORS_FORMAT, 1)));
        assertEquals("Errors should contain metadata for first trigger with errors", errors.get(RepoSettings.KEY_META_FIRST_TRIGGER_WITH_ERRORS), "1");
        errors.clear();
    }

    @Test
    public void testItem() {
        final SettingsErrors errors = new SettingsErrors();
        final Settings settings = mock(Settings.class);
        when(settings.getInt(eq(RepoSettings.KEY_CONFIG_COUNT), Mockito.anyInt())).thenReturn(1);
        when(settings.getString(eq(KEY_BUILD_CONFIG_1), Mockito.anyString())).thenReturn("buildConfig");
        when(settings.getString(eq(KEY_DEFAULT_BRANCH_1), Mockito.anyString())).thenReturn("refs/heads/develop");
        when(settings.getString(eq(KEY_ADDITIONAL_BRANCHES_FILTER_1), Mockito.anyString())).thenReturn(" ");

        final MinimalRef ref = mock(MinimalRef.class);

        RepoSettings repoSettings = new RepoSettings(settings, errors, null);
        assertTrue("RepoSettings should have no errors", errors.isEmpty());
        RepoSettings.Item item = repoSettings.getItems().get(0);

        when(ref.getId()).thenReturn("refs/heads/develop");
        assertTrue("It should match default branch", item.appliesTo(ref));

        when(ref.getId()).thenReturn("refs/heads/master");
        assertFalse("It should not match another branch", item.appliesTo(ref));

        when(settings.getString(eq(KEY_ADDITIONAL_BRANCHES_FILTER_1), Mockito.anyString())).thenReturn("master");

        repoSettings = new RepoSettings(settings, errors, null);
        assertTrue("RepoSettings should have no errors", errors.isEmpty());
        item = repoSettings.getItems().get(0);

        when(ref.getId()).thenReturn("refs/heads/develop");
        assertTrue("It should still match default branch", item.appliesTo(ref));

        when(ref.getId()).thenReturn("refs/heads/master");
        assertTrue("It should also match additional branch", item.appliesTo(ref));

        when(ref.getId()).thenReturn("refs/heads/release");
        assertFalse("It should not match another branch", item.appliesTo(ref));

        when(settings.getString(eq(KEY_ADDITIONAL_BRANCHES_FILTER_1), Mockito.anyString())).thenReturn("refs/pull/.*/merge");

        repoSettings = new RepoSettings(settings, errors, null);
        assertTrue("RepoSettings should have no errors", errors.isEmpty());
        item = repoSettings.getItems().get(0);

        when(ref.getId()).thenReturn("refs/pull/1/merge");
        assertTrue("It should also match pull branch", item.appliesTo(ref));
    }
}
