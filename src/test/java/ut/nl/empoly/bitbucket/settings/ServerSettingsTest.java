package ut.nl.empoly.bitbucket.settings;


import nl.empoly.bitbucket.settings.ServerSettings;
import nl.empoly.bitbucket.settings.SettingsErrors;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.ServletRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by roy on 01-11-16.
 */
public class ServerSettingsTest {

    @Test
    public void testAddressFormatting() {
        final ServerSettings serverSettings = new ServerSettings(null);

        serverSettings.setAddress("test");
        assertEquals("Address should be decorated with schema", serverSettings.getAddress(), "http://test/");

        serverSettings.setAddress("test///");
        assertEquals("Settings should trim trailing slashes", serverSettings.getAddress(), "http://test/");

        serverSettings.setAddress("https://test:3000");
        assertEquals("Settings should keep schema if specified", serverSettings.getAddress(), "https://test:3000/");

        serverSettings.setAddress(" ");
        assertEquals("Blank address should not be decorated", serverSettings.getAddress(), "");
    }


    @Test
    public void testAddressValidation() {
        final SettingsErrors errors = new SettingsErrors();
        final ServerSettings serverSettings = new ServerSettings(null);

        serverSettings.validate(errors, null);
        assertTrue("Empty address should result in error", errors.containsKey(ServerSettings.KEY_ADDRESS));
        errors.clear();

        serverSettings.setAddress("  ");
        serverSettings.validate(errors, null);
        assertTrue("Blank address should result in error", errors.containsKey(ServerSettings.KEY_ADDRESS));
        errors.clear();

        serverSettings.setAddress("http://teamcity");
        serverSettings.validate(errors, null);
        assertFalse("Non-blank address should not result in error", errors.containsKey(ServerSettings.KEY_ADDRESS));
    }


    @Test
    public void testUsernameTrimming() {
        final ServerSettings serverSettings = new ServerSettings(null);
        serverSettings.setUsername(" username ");
        assertEquals("Username should be trimmed", serverSettings.getUsername(), "username");
    }

    @Test
    public void testPasswordReset() {
        final ServerSettings serverSettings = new ServerSettings(null);
        final ServletRequest request = mock(ServletRequest.class);
        when(request.getParameter(Mockito.anyString())).thenReturn("");

        serverSettings.setAddress("http://teamcity");
        serverSettings.setUsername("username");
        serverSettings.setPassword("password");
        assertEquals("Password should be persisted", serverSettings.getPassword(), "password");

        when(request.getParameter(ServerSettings.KEY_USERNAME)).thenReturn("username");
        serverSettings.update(request);
        assertEquals("Password should not change", serverSettings.getPassword(), "password");

        when(request.getParameter(ServerSettings.KEY_USERNAME)).thenReturn("username2");
        serverSettings.update(request);
        assertEquals("Password should change", serverSettings.getPassword(), "");
    }

}
