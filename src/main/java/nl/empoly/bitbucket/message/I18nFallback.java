package nl.empoly.bitbucket.message;

import com.atlassian.sal.core.message.AbstractI18nResolver;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

/**
 * Created by roy on 30-10-16.
 */
public class I18nFallback extends AbstractI18nResolver {
    public String resolveText(String s, Serializable[] serializables) {
        return s;
    }

    public String resolveText(Locale locale, String s, Serializable[] serializables) {
        return s;
    }

    public String getRawText(String s) {
        return s;
    }

    public String getRawText(Locale locale, String s) {
        return s;
    }

    public Map<String, String> getAllTranslationsForPrefix(String s) {
        return null;
    }

    public Map<String, String> getAllTranslationsForPrefix(String s, Locale locale) {
        return null;
    }
}
