package nl.empoly.bitbucket.settings;

import com.atlassian.bitbucket.ui.ValidationErrors;

import javax.annotation.Nonnull;
import java.util.HashMap;

/**
 * Created by roy on 30-10-16.
 */
public class SettingsErrors extends HashMap<String, String> implements ValidationErrors {

    public void addFieldError(@Nonnull String field, @Nonnull String error) {
        put(field, error);
    }

    public void addFormError(@Nonnull String error) {
        put(null, error);
    }

    public boolean arePresent() {
        return !isEmpty();
    }

}
