package nl.empoly.bitbucket.settings;

import com.atlassian.bitbucket.repository.MinimalRef;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.ui.ValidationErrors;
import com.atlassian.sal.api.message.I18nResolver;
import nl.empoly.bitbucket.message.I18nFallback;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Created by roy on 29-10-16.
 */
public class RepoSettings {
    public static final int MAX_TRIGGER_COUNT = 7;

    public static final String KEY_CONFIG_COUNT = "meta[configCount]";
    public static final String KEY_BUILD_CONFIG_FORMAT = "trigger[%d][buildConfig]";
    public static final String KEY_DEFAULT_BRANCH_FORMAT = "trigger[%d][defaultBranch]";
    public static final String KEY_ADDITIONAL_BRANCHES_FILTER_FORMAT = "trigger[%d][additionalBranchesFilter]";

    public static final String KEY_META_TRIGGER_HAS_ERRORS_FORMAT = "meta--trigger-%d-has-errors";
    public static final String KEY_META_FIRST_TRIGGER_WITH_ERRORS = "meta--first-trigger-with-errors";

    private final List<Item> items;

    public RepoSettings(@Nonnull Settings settings, @Nonnull ValidationErrors errors, I18nResolver i18n) {
        if (i18n == null) {
            i18n = new I18nFallback();
        }

        int triggerCount;
        try {
            triggerCount = settings.getInt(KEY_CONFIG_COUNT, 1);
        } catch (Exception e) {
            triggerCount = 1;
        }

        if (triggerCount > MAX_TRIGGER_COUNT) {
            errors.addFormError(i18n.getText("teamcity-trigger.error.max_triggers_exceeded", MAX_TRIGGER_COUNT));
        }
        if (triggerCount < 1) {
            errors.addFormError(i18n.getText("teamcity-trigger.error.no_triggers_defined", MAX_TRIGGER_COUNT));
        }

        items = new ArrayList<Item>(triggerCount);
        boolean firstError = true;
        for (int i = 0; i < triggerCount; i++) {
            int index = i + 1;
            boolean valid = true;
            String buildConfigKey = String.format(Locale.US, KEY_BUILD_CONFIG_FORMAT, index);
            String defaultBranchKey = String.format(Locale.US, KEY_DEFAULT_BRANCH_FORMAT, index);
            String branchFilterKey = String.format(Locale.US, KEY_ADDITIONAL_BRANCHES_FILTER_FORMAT, index);
            String buildConfig = settings.getString(buildConfigKey, "");
            if (buildConfig.trim().length() == 0) {
                errors.addFieldError(buildConfigKey, i18n.getText("teamcity-trigger.error.build_config_blank"));
                valid = false;
            }
            String defaultBranch = settings.getString(defaultBranchKey, "");
            if (defaultBranch.trim().length() == 0) {
                errors.addFieldError(defaultBranchKey, i18n.getText("teamcity-trigger.error.default_branch_blank"));
                valid = false;
            }
            String branchFilterString = settings.getString(branchFilterKey, "");
            Pattern additionalBranchesFilter = null;
            if (branchFilterString.trim().length() > 0) {
                try {
                    additionalBranchesFilter = Pattern.compile(branchFilterString);
                } catch (PatternSyntaxException e) {
                    errors.addFieldError(branchFilterKey, i18n.getText("teamcity-trigger.error.invalid_regex"));
                    valid = false;
                }
            }
            if (valid) {
                items.add(new Item(buildConfig, defaultBranch, additionalBranchesFilter));
            } else {
                errors.addFieldError(String.format(Locale.US, KEY_META_TRIGGER_HAS_ERRORS_FORMAT, index), "true");
                if (firstError) {
                    errors.addFieldError(KEY_META_FIRST_TRIGGER_WITH_ERRORS, String.valueOf(index));
                    firstError = false;
                }
            }
        }
    }

    public List<Item> getItems() {
        return items;
    }

    public static class Item {
        Item(@Nonnull String buildConfigId, @Nonnull String defaultBranch, Pattern additionalBranchesFilter) {
            this.buildConfigId = buildConfigId;
            this.defaultBranch = defaultBranch;
            this.additionalBranchesFilter = additionalBranchesFilter;
        }

        public final String buildConfigId;
        public final String defaultBranch;
        public final Pattern additionalBranchesFilter;

        public boolean appliesTo(MinimalRef ref) {
            final String refId = ref.getId();
            return refId.equals(defaultBranch) || (additionalBranchesFilter != null &&
                    additionalBranchesFilter.matcher(refId.replaceFirst("^refs/heads/", "")).matches());
        }
    }
}
