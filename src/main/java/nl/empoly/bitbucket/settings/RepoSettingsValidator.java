package nl.empoly.bitbucket.settings;

import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;
import com.atlassian.bitbucket.setting.SettingsValidator;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;

import javax.annotation.Nonnull;

/**
 * Created by roy on 06-10-17.
 */
public class RepoSettingsValidator implements SettingsValidator {
    private final I18nResolver i18n;

    public RepoSettingsValidator(@ComponentImport I18nResolver i18n) {
        this.i18n = i18n;
    }

    public void validate(@Nonnull Settings settings, @Nonnull SettingsValidationErrors errors, @Nonnull Scope scope) {
        new RepoSettings(settings, errors, i18n);
    }
}
