package nl.empoly.bitbucket.settings;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import nl.empoly.bitbucket.message.I18nFallback;

import javax.annotation.Nonnull;
import javax.servlet.ServletRequest;

/**
 * Created by roy on 23-10-16.
 */
public class ServerSettings {
    private static final String SETTINGS_BASE = "nl.empoly.bitbucket.teamcity-trigger.server:";
    private static final String SETTINGS_ADDRESS = SETTINGS_BASE + "address";
    private static final String SETTINGS_NAME = SETTINGS_BASE + "name";
    private static final String SETTINGS_PASSWORD = SETTINGS_BASE + "password";

    public static final String KEY_ADDRESS = "server[address]";
    public static final String KEY_USERNAME = "server[username]";
    public static final String KEY_PASSWORD = "server[password]";

    private String address = "";
    private String username = "";
    private String password = "";

    public ServerSettings(PluginSettings settings) {
        if (settings != null) {
            Object address = settings.get(SETTINGS_ADDRESS);
            Object username = settings.get(SETTINGS_NAME);
            Object password = settings.get(SETTINGS_PASSWORD);

            this.address = address == null ? "" : address.toString();
            this.username = username == null ? "" : username.toString();
            this.password = password == null ? "" : password.toString();
        }
    }

    public void update(@Nonnull ServletRequest request) {
        setAddress(request.getParameter(KEY_ADDRESS));

        String username = request.getParameter(KEY_USERNAME);
        String password = request.getParameter(KEY_PASSWORD);
        String oldUsername = getUsername();
        setUsername(username);
        if (password.length() > 0 || !oldUsername.equalsIgnoreCase(username)) {
            setPassword(password);
        }
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        if (address == null || address.trim().length() == 0) {
            address = "";
        } else {
            address = address.trim().replaceFirst("/*$", "/");
            if (!address.contains("://")) {
                address = "http://" + address;
            }
        }
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? "" : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? "" : password;
    }

    public boolean validate(@Nonnull SettingsErrors errors, I18nResolver i18n) {
        if (i18n == null) {
            i18n = new I18nFallback();
        }
        if (address.length() == 0) {
            errors.addFieldError(KEY_ADDRESS, i18n.getText("teamcity-trigger-configuration.error.address_empty"));
        }
        return errors.isEmpty();
    }

    public void save(@Nonnull PluginSettings settings) {
        settings.put(SETTINGS_ADDRESS, getAddress());
        settings.put(SETTINGS_NAME, getUsername());
        settings.put(SETTINGS_PASSWORD, getPassword());
    }
}
