package nl.empoly.bitbucket.servlet;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import nl.empoly.bitbucket.settings.ServerSettings;
import nl.empoly.bitbucket.settings.SettingsErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Component
public class ServerConfigurationServlet extends HttpServlet {
    private final I18nResolver i18n;
    private final SoyTemplateRenderer renderer;
    private final PluginSettingsFactory settingsFactory;
    private final UserManager userManager;
    private final LoginUriProvider loginUriProvider;

    @Autowired
    public ServerConfigurationServlet(@ComponentImport SoyTemplateRenderer renderer, @ComponentImport I18nResolver i18n, @ComponentImport PluginSettingsFactory settingsFactory, @ComponentImport UserManager userManager, @ComponentImport LoginUriProvider loginUriProvider) {
        this.i18n = i18n;
        this.renderer = renderer;
        this.settingsFactory = settingsFactory;
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserKey user = userManager.getRemoteUserKey(request);
        if (user == null || !userManager.isAdmin(user)) {
            redirectToLogin(request, response);
            return;
        }

        Map<String, Object> data = new HashMap<String, Object>(1);
        data.put("config", new ServerSettings(settingsFactory.createGlobalSettings()));

        String returnTo = request.getParameter("return-to");
        if (returnTo != null && returnTo.length() > 0) {
            data.put("returnTo", returnTo);
        }

        render(response, "nl.empoly.bitbucket.template.servlet.serverConfiguration", data);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserKey user = userManager.getRemoteUserKey(request);
        if (user == null || !userManager.isAdmin(user)) {
            redirectToLogin(request, response);
            return;
        }

        PluginSettings pluginSettings = settingsFactory.createGlobalSettings();

        final ServerSettings settings = new ServerSettings(pluginSettings);
        settings.update(request);
        SettingsErrors errors = new SettingsErrors();
        settings.validate(errors, i18n);

        String returnTo = request.getParameter("return-to");
        if (errors.isEmpty()) {
            settings.save(pluginSettings);
            if (returnTo != null && returnTo.length() > 0 && !returnTo.contains(".")) {
                response.sendRedirect(returnTo);
            } else {
                response.sendRedirect(request.getContextPath() + "/plugins/servlet/upm");
            }
        } else {
            Map<String, Object> data = new HashMap<String, Object>(1);
            data.put("config", settings);
            data.put("errors", errors);
            if (returnTo != null && returnTo.length() > 0) {
                data.put("returnTo", returnTo);
            }
            render(response, "nl.empoly.bitbucket.template.servlet.serverConfiguration", data);
        }
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }

        URI loginUri = URI.create(builder.toString());
        response.sendRedirect(loginUriProvider.getLoginUri(loginUri).toASCIIString());
    }

    private void render(HttpServletResponse response, String templateName, Map<String, Object> data) throws IOException, ServletException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            renderer.render(response.getWriter(),
                    "nl.empoly.bitbucket.teamcity-trigger:server-configuration-templates",
                    templateName, data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }

}