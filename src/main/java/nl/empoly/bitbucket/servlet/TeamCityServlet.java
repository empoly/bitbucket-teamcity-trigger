package nl.empoly.bitbucket.servlet;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import nl.empoly.bitbucket.hook.TeamCityTriggerHook;
import nl.empoly.bitbucket.settings.ServerSettings;
import nl.empoly.bitbucket.teamcity.TeamCityClient;
import nl.empoly.bitbucket.teamcity.TeamCityResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

/**
 * Created by roy on 23-10-16.
 */
@Component
public class TeamCityServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(TeamCityTriggerHook.class);

    private final I18nResolver i18n;
    private final PluginSettingsFactory settingsFactory;
    private final UserManager userManager;

    @Autowired
    public TeamCityServlet(@ComponentImport I18nResolver i18n, @ComponentImport PluginSettingsFactory settingsFactory, @ComponentImport UserManager userManager) {
        this.i18n = i18n;
        this.settingsFactory = settingsFactory;
        this.userManager = userManager;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserKey user = userManager.getRemoteUserKey(request);
        if (user == null) {
            response.sendError(401, i18n.getText("teamcity-trigger-connector.error.please_login"));
            return;
        }

        final ServerSettings settings = new ServerSettings(settingsFactory.createGlobalSettings());
        if (settings.getAddress().length() == 0) {
            response.setStatus(422);
            response.setContentType("application/json");
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.address_empty") + "\"}");
            return;
        }

        try {
            TeamCityClient client = new TeamCityClient(settings);

            String path = request.getPathInfo();
            if (path.equals("/search")) {
                String query = request.getParameter("q");
                searchBuildTypes(client, query, response);

            } else if (path.startsWith("/info/")) {
                String slug = path.substring(6);
                if (slug.contains("/")) {
                    response.sendError(404);
                    return;
                }
                getBuildType(client, slug, response);
            } else {
                response.sendError(404);
            }
        } catch (MalformedURLException e) {
            response.setStatus(502);
            response.setContentType("application/json");
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.address_malformed") + "\"}");
        } catch (UnknownHostException e) {
            response.setStatus(502);
            response.setContentType("application/json");
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.unknown_host") + "\"}");
        } catch (SocketTimeoutException e) {
            response.setStatus(502);
            response.setContentType("application/json");
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.socket_timeout") + "\"}");
        } catch (ConnectException e) {
            response.setStatus(502);
            response.setContentType("application/json");
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.connection_failed") + "\"}");
        }
    }

    private void searchBuildTypes(TeamCityClient client, String query, HttpServletResponse response) throws IOException {
        TeamCityResponse teamCityResponse = client.getBuildTypes();
        if (teamCityResponse.isSuccessful()) {
            Pattern[] matchers;
            if (query == null || query.trim().length() == 0) {
                matchers = null;
            } else {
                String[] words = query.trim().split("\\s+");
                matchers = new Pattern[words.length];
                for (int i = 0; i < words.length; i++) {
                    matchers[i] = Pattern.compile(words[i], Pattern.LITERAL | Pattern.CASE_INSENSITIVE);
                }
            }
            try {
                StringBuilder responseBuilder = new StringBuilder();
                responseBuilder.append("[\n  ");

                Document document = teamCityResponse.getBody();
                NodeList buildTypes = document.getElementsByTagName("buildType");
                Node node;
                Element element;
                boolean first = true;
                for (int i = 0; i < buildTypes.getLength(); i++) {
                    node = buildTypes.item(i);
                    if (node.getNodeType() != Node.ELEMENT_NODE) {
                        continue;
                    }
                    element = (Element) node;
                    if (element.getAttribute("paused").equals("true")) {
                        continue;
                    }
                    String id = element.getAttribute("id");
                    String name = element.getAttribute("name");
                    String projectName = element.getAttribute("projectName");
                    if (matchers != null) {
                        boolean matches = true;
                        String matchInput = id + " " + name + " " + projectName;
                        for (Pattern matcher : matchers) {
                            if (!matcher.matcher(matchInput).find()) {
                                matches = false;
                                break;
                            }
                        }
                        if (!matches) {
                            continue;
                        }
                    }
                    if (first) {
                        first = false;
                    } else {
                        responseBuilder.append(",\n  ");
                    }
                    responseBuilder.append("{\n" +
                            "  \"id\": \"" + id + "\",\n" +
                            "  \"name\": \"" + name + "\",\n" +
                            "  \"description\": \"" + element.getAttribute("description") + "\",\n" +
                            "  \"projectName\": \"" + projectName + "\"\n" +
                            "}");
                }

                responseBuilder.append("\n]");
                response.setContentType("application/json");
                response.getWriter().write(responseBuilder.toString());
            } catch (SAXException e) {
                LOG.error("Could not parse TeamCity response", e);
                response.sendError(502, i18n.getText("teamcity-trigger-connector.error.cannot_parse_teamcity_response"));
            } finally {
                teamCityResponse.close();
            }
        } else if (teamCityResponse.getResponseCode() == 401) {
            response.setContentType("application/json");
            response.setStatus(502);
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.invalid_credentials") + "\"}");
        } else {
            response.setStatus(502);
            response.setContentType("application/json");
            response.getWriter().write("{\n" +
                    "  \"error\": \"" + i18n.getText("teamcity-trigger-connector.error.teamcity_error", teamCityResponse.getResponseCode(), teamCityResponse.getResponseMessage()) + "\",\n" +
                    "  \"status_code\": \"" + teamCityResponse.getResponseCode() + "\"\n" +
                    "  \"status_message\": \"" + teamCityResponse.getResponseMessage() + "\"\n" +
                    "}");
        }
    }

    private void getBuildType(TeamCityClient client, String id, HttpServletResponse response) throws IOException {
        TeamCityResponse teamCityResponse = client.getBuildType(id);
        if (teamCityResponse.isSuccessful()) {
            try {
                Document document = teamCityResponse.getBody();
                if (document.getDocumentElement().getAttribute("templateFlag").equals("true")) {
                    response.setContentType("application/json");
                    response.getWriter().write("{\"message\": \"" + i18n.getText("teamcity-trigger-connector.this_is_a_template") + "\"}");
                } else {
                    String buildTypeInfo = "{\n" +
                            "  \"buildConfig\": {\n" +
                            "    \"name\": \"" + document.getDocumentElement().getAttribute("name") + "\",\n" +
                            "    \"description\": \"" + document.getDocumentElement().getAttribute("description") + "\",\n" +
                            "    \"projectName\": \"" + document.getDocumentElement().getAttribute("projectName") + "\"\n" +
                            "  }\n" +
                            "}";

                    response.setContentType("application/json");
                    response.getWriter().write(buildTypeInfo);
                }
            } catch (SAXException e) {
                LOG.error("Could not parse TeamCity response", e);
                response.sendError(502, i18n.getText("teamcity-trigger-connector.error.cannot_parse_teamcity_response"));
            } finally {
                teamCityResponse.close();
            }
        } else if (teamCityResponse.getResponseCode() == 404) {
            response.setContentType("application/json");
            response.getWriter().write("{\"message\": \"" + i18n.getText("teamcity-trigger-connector.build_config_does_not_exist") + "\"}");
        } else if (teamCityResponse.getResponseCode() == 401) {
            response.setContentType("application/json");
            response.setStatus(502);
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.invalid_credentials") + "\"}");
        } else {
            response.setStatus(502);
            response.setContentType("application/json");
            response.getWriter().write("{\n" +
                    "  \"error\": \"" + i18n.getText("teamcity-trigger-connector.error.teamcity_error", teamCityResponse.getResponseCode(), teamCityResponse.getResponseMessage()) + "\",\n" +
                    "  \"status_code\": \"" + teamCityResponse.getResponseCode() + "\",\n" +
                    "  \"status_message\": \"" + teamCityResponse.getResponseMessage() + "\"\n" +
                    "}");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserKey user = userManager.getRemoteUserKey(request);
        if (user == null) {
            response.sendError(401, i18n.getText("teamcity-trigger-connector.error.please_login"));
            return;
        }

        if (!request.getPathInfo().equals("/test")) {
            response.sendError(404);
            return;
        }

        ServerSettings settings = new ServerSettings(settingsFactory.createGlobalSettings());
        String savedUsername = settings.getUsername();
        String password = request.getParameter("password");

        settings.setAddress(request.getParameter("address"));
        settings.setUsername(request.getParameter("username"));
        if (!savedUsername.equalsIgnoreCase(settings.getUsername()) || (password != null && password.length() > 0)) {
            settings.setPassword(password);
        }

        if (settings.getAddress().length() == 0) {
            response.setStatus(422);
            response.setContentType("application/json");
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.address_empty") + "\"}");
            return;
        }

        TeamCityResponse teamCityResponse = null;
        try {
            TeamCityClient client = new TeamCityClient(settings);
            teamCityResponse = client.getBuildTypes();
            if (teamCityResponse.isSuccessful()) {
                response.setStatus(204);
            } else if (teamCityResponse.getResponseCode() == 401) {
                response.setStatus(401);
                response.setContentType("application/json");
                response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.invalid_credentials") + "\"}");
            } else if (teamCityResponse.getResponseCode() == 404) {
                response.setStatus(422);
                response.setContentType("application/json");
                response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.api_not_found") + "\"}");
            } else {
                response.setStatus(502);
                response.setContentType("application/json");
                response.getWriter().write("{\n" +
                        "  \"error\": \"" + i18n.getText("teamcity-trigger-connector.error.teamcity_error", teamCityResponse.getResponseCode(), teamCityResponse.getResponseMessage()) + "\",\n" +
                        "  \"status_code\": \"" + teamCityResponse.getResponseCode() + "\",\n" +
                        "  \"status_message\": \"" + teamCityResponse.getResponseMessage() + "\"\n" +
                        "}");
            }
        } catch (MalformedURLException e) {
            response.setStatus(422);
            response.setContentType("application/json");
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.address_malformed") + "\"}");
        } catch (UnknownHostException e) {
            response.setStatus(422);
            response.setContentType("application/json");
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.unknown_host") + "\"}");
        } catch (SocketTimeoutException e) {
            response.setStatus(422);
            response.setContentType("application/json");
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.socket_timeout") + "\"}");
        } catch (ConnectException e) {
            response.setStatus(422);
            response.setContentType("application/json");
            response.getWriter().write("{\"error\": \"" + i18n.getText("teamcity-trigger-connector.error.connection_failed") + "\"}");
        } finally {
            if (teamCityResponse != null) {
                teamCityResponse.close();
            }
        }
    }
}
