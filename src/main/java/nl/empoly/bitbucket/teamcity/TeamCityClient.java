package nl.empoly.bitbucket.teamcity;

import nl.empoly.bitbucket.settings.ServerSettings;
import org.apache.commons.codec.binary.Base64;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class TeamCityClient {
    private final static String USER_AGENT = "BitbucketHook/TeamCityTrigger";

    private final ServerSettings settings;

    public TeamCityClient(ServerSettings settings) {
        this.settings = settings;
    }

    private URL getBuildTypesUrl() throws MalformedURLException {
        return new URL(settings.getAddress() + "httpAuth/app/rest/buildTypes");
    }

    private URL getBuildTypeUrl(String locator) throws MalformedURLException {
        return new URL(settings.getAddress() + "httpAuth/app/rest/buildTypes/id:" + locator);
    }

    private URL getBuildTriggerUrl() throws MalformedURLException {
        return new URL(settings.getAddress() + "httpAuth/app/rest/buildQueue/");
    }

    private void setAuthorization(URLConnection urlConnection) {
        if (settings.getUsername().length() > 0) {
            // build the auth string
            String authString = settings.getUsername() + ":" + settings.getPassword();
            String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
            urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
        }
    }

    public TeamCityResponse getBuildTypes() throws IOException {
        URL url = getBuildTypesUrl();
        URLConnection connection = url.openConnection();

        setAuthorization(connection);
        connection.setReadTimeout(2000);
        connection.setRequestProperty("User-Agent", USER_AGENT);
        ((HttpURLConnection) connection).setRequestMethod("GET");

        connection.connect();
        return new TeamCityResponse((HttpURLConnection) connection);
    }

    public TeamCityResponse getBuildType(String id) throws IOException {
        URL url = getBuildTypeUrl(id);
        URLConnection connection = url.openConnection();

        setAuthorization(connection);
        connection.setReadTimeout(2000);
        connection.setRequestProperty("User-Agent", USER_AGENT);
        ((HttpURLConnection) connection).setRequestMethod("GET");

        connection.connect();
        return new TeamCityResponse((HttpURLConnection) connection);
    }

    public TeamCityResponse triggerBuild(String locator, String branchName) throws IOException {
        String request = "<build" +
                (branchName != null ? " branchName=\"" + branchName + "\"" : "") + ">\n" +
                "    <buildType id=\"" + locator + "\"/>\n" +
                "</build>";

        URL url = getBuildTriggerUrl();
        URLConnection connection = url.openConnection();

        setAuthorization(connection);
        connection.setReadTimeout(6000);
        connection.setRequestProperty("User-Agent", USER_AGENT);
        ((HttpURLConnection) connection).setRequestMethod("POST");

        connection.setRequestProperty("Content-Type", "application/xml");
        connection.setRequestProperty("Content-Length", "" + Integer.toString(request.getBytes().length));

        connection.setDoInput(true);
        connection.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes(request);
        wr.flush();
        wr.close();

        return new TeamCityResponse((HttpURLConnection) connection);
    }
}
