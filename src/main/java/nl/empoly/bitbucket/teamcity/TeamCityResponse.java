package nl.empoly.bitbucket.teamcity;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

/**
 * Created by roy on 24-10-16.
 */
public class TeamCityResponse {
    private final DocumentBuilderFactory documentBuilders = DocumentBuilderFactory.newInstance();

    private final HttpURLConnection connection;

    TeamCityResponse(HttpURLConnection connection) {
        this.connection = connection;
    }

    public int getResponseCode() throws IOException {
        return connection.getResponseCode();
    }

    public String getResponseMessage() throws IOException {
        return connection.getResponseMessage();
    }

    public boolean isSuccessful() {
        try {
            int responseCode = getResponseCode();
            return responseCode >= 200 && responseCode < 400;
        } catch (IOException e) {
            return false;
        }
    }

    public Document getBody() throws IOException, SAXException {
        try {
            DocumentBuilder builder = documentBuilders.newDocumentBuilder();
            InputStream inputStream = connection.getInputStream();
            Document document = builder.parse(inputStream);
            inputStream.close();
            return document;
        } catch (ParserConfigurationException e) {
            return null;
        }
    }

    public void close() {
        connection.disconnect();
    }
}
