package nl.empoly.bitbucket.hook;

import com.atlassian.bitbucket.hook.repository.PostRepositoryHook;
import com.atlassian.bitbucket.hook.repository.PostRepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.RepositoryHookRequest;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.StandardRefType;
import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;
import com.atlassian.bitbucket.setting.SettingsValidator;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import nl.empoly.bitbucket.settings.RepoSettings;
import nl.empoly.bitbucket.settings.ServerSettings;
import nl.empoly.bitbucket.settings.SettingsErrors;
import nl.empoly.bitbucket.teamcity.TeamCityClient;
import nl.empoly.bitbucket.teamcity.TeamCityResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.IOException;

@Component
public class TeamCityTriggerHook implements PostRepositoryHook<RepositoryHookRequest> {
    private static final Logger LOG = LoggerFactory.getLogger(TeamCityTriggerHook.class);

    private final PluginSettingsFactory settingsFactory;

    @Autowired
    public TeamCityTriggerHook(@ComponentImport PluginSettingsFactory settingsFactory) {
        this.settingsFactory = settingsFactory;
    }

    public void postUpdate(@Nonnull PostRepositoryHookContext context, @Nonnull RepositoryHookRequest request) {
        final ServerSettings serverSettings = new ServerSettings(settingsFactory.createGlobalSettings());
        if (!serverSettings.validate(new SettingsErrors(), null)) {
            LOG.error("Abort postReceive: Invalid server settings.");
        }

        final TeamCityClient teamCityClient = new TeamCityClient(serverSettings);

        final SettingsErrors errors = new SettingsErrors();
        final RepoSettings settings = new RepoSettings(context.getSettings(), errors, null);
        if (errors.arePresent()) {
            LOG.error("Some trigger configurations are invalid will be skipped.");
        }


        for (RefChange refChange : request.getRefChanges()) {
            if (refChange.getRef().getType() != StandardRefType.BRANCH) {
                continue;
            }
            for (RepoSettings.Item item : settings.getItems()) {
                if (item.appliesTo(refChange.getRef())) {
                    final String branchName = refChange.getRef().getId();
                    LOG.info("Triggering build config '" + item.buildConfigId + "' for branch '" + branchName + "'");
                    try {
                        TeamCityResponse response = teamCityClient.triggerBuild(item.buildConfigId,
                                branchName.equals(item.defaultBranch) ? null : branchName
                        );
                        if (response.getResponseCode() >= 400) {
                            LOG.error("Triggering build failed: HTTP " + response.getResponseCode() + " - " + response.getResponseMessage());
                        }
                    } catch (IOException e) {
                        LOG.error("Could not reach TeamCity Server", e);
                    }
                }
            }
        }
    }
}