'use strict';
var TeamCityTrigger = {};
Object.defineProperty(TeamCityTrigger, "maxTriggerCount", { value: 7, writable: false });
Object.defineProperty(TeamCityTrigger, "repoConfigurationSelector", {value: '#teamcity-trigger-configuration', writable: false});

TeamCityTrigger.initServerConfiguration = function () {
    AJS.$('input#server_username').keyup(function() {
        $input = AJS.$(this);
        if ($input.val() == $input.attr('persistedvalue') && AJS.$('input#server_password').attr("ispersisted")){
            AJS.$('input#server_password').attr('placeholder', AJS.I18n.getText("teamcity-trigger-configuration.dont_change_password"))
        } else {
            AJS.$('input#server_password').attr('placeholder', '');
        }
    });
};

TeamCityTrigger.testServerConfiguration = function () {
    var $addressInput = AJS.$('input#server_address'),
        address = $addressInput.val(),
        username = AJS.$('input#server_username').val(),
        password = AJS.$('input#server_password').val(),
        data = {address: address, username: username},
        progressFlag;

    if (address.length == 0) {
        AJS.flag({
            type: 'error',
            title: AJS.I18n.getText("teamcity-trigger-configuration.error.address_empty"),
            close: 'auto'
        });
        $addressInput.attr("data-aui-notification-field", '').attr("data-aui-notification-error", '');
        return;
    } else {
        $addressInput.attr("data-aui-notification-field", null).attr("data-aui-notification-error", null);
    }
    if (password.length > 0){
        data.password = password;
    }

    progressFlag = AJS.flag({
        type: 'info',
        title: AJS.I18n.getText("teamcity-trigger-configuration.test.progress_title"),
        body: '<div id="some-id" class="aui-progress-indicator">' +
            '  <span class="aui-progress-indicator-value"></span>' +
            '</div>',
        close: 'never'
    });
    AJS.$.ajax({
        type: "POST",
        url: AJS.contextPath() + "/plugins/servlet/teamcity-trigger/connector/test",
        data: data,
        success: function(data){
            AJS.flag({
                type: 'success',
                title: AJS.I18n.getText("teamcity-trigger-configuration.test.success_title"),
                body: AJS.I18n.getText("teamcity-trigger-configuration.test.success_body"),
                close: 'auto'
            });
        },
        error: function(jqXHR, textStatus){
            var flagBody = AJS.I18n.getText("teamcity-trigger-configuration.error.unknown_test_error") + " &#128540;";
            if (textStatus === "timeout") {
                flagBody = AJS.I18n.getText("teamcity-trigger-configuration.error.test_timeout");
            } else {
                try {
                    var response = JSON.parse(jqXHR.responseText);
                    if (response.error) {
                        flagBody = response.error;
                    }
                } catch(err) {
                    console.error(err);
                }
            }
            AJS.flag({
                type: 'error',
                title: AJS.I18n.getText("teamcity-trigger-configuration.test.error_title"),
                body: flagBody,
                close: 'manual'
            });
        },
        complete: function (){
          progressFlag.remove();
        }
    });
};

TeamCityTrigger.initRepoConfiguration = function () {
    var $form = AJS.$(TeamCityTrigger.repoConfigurationSelector);
    if ($form.length === 0) {
         console.error('TeamCity trigger: Repo configuration init failed, form not found.');
         return;
    }

    AJS.tabs.setup();
    $form.find('.hook-config-contents').css('min-height', '300px');
    $form.find('a#add-tab').on('click', TeamCityTrigger.addRepoConfigurationTrigger);
    TeamCityTrigger.initRepoConfigurationTabsPane($form.find('.tabs-pane'));
    TeamCityTrigger.updateRepoConfigurationButtons($form.find('.teamcity-triggers > .tabs-menu > .menu-item').length);
};

TeamCityTrigger.initRepoConfigurationTabsPane = function ($panes) {
    $panes.find('input.trigger_name').on('keyup change', function() {
        var $input = AJS.$(this),
            $menuItemLink = AJS.$(TeamCityTrigger.repoConfigurationSelector).find('a[href="#'+ $input.closest('.tabs-pane').attr('id') +'"]'),
            $menuItem = $menuItemLink.closest('.menu-item'),
            value = $input.val();
        if (value.length) {
            $menuItemLink.text(value);
            $menuItem.attr('title', value);
        } else {
            $menuItemLink.text($input.attr('placeholder'));
            $menuItem.attr('title', $input.attr('placeholder'));
        }
    });
    $panes.find('input.trigger_buildConfig').auiSelect2({
        ajax: {
            url: AJS.contextPath() + "/plugins/servlet/teamcity-trigger/connector/search",
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function (params) {
                return { q: params };
            },
            results: function (data, page) {
                AJS.$('#teamcity_connector_error').remove();
                return { results: data };
            },
            params: {
              error: function (jqXHR) {
                  var errorMessage;
                  try {
                      var response = JSON.parse(jqXHR.responseText);
                      errorMessage = response.error + '.';
                  } catch(err) {
                      console.error(err);
                      errorMessage = '';
                  }
                  errorMessage += ' <a href="' + AJS.contextPath() + '/plugins/servlet/teamcity-trigger/configure?return-to=' + window.location.pathname + '">' + AJS.I18n.getText("teamcity-trigger.server_configuration") + '</a>';

                  AJS.$('#teamcity_connector_error').remove();
                  AJS.messages.error('#form-message-bar', {
                      id: 'teamcity_connector_error',
                      title: AJS.I18n.getText("teamcity-trigger.error.connector_error"),
                      body: errorMessage,
                      closeable: false
                  });
              }
            }
        },
        initSelection: function(element, callback) {
            callback({id: element.val()});
        },
        formatResult: function (data) {
            return '<div class="teamcity-trigger-buildconfig-suggestion">' +
              ' <div class="suggestion-title'+ (data.description.length ? '' : ' no-description') +'">'+
              '  <span class="buildconfig-project">' + data.projectName +' /</span> '+
              '  <span class="buildconfig-name">' + data.name +'</span>'+
              ' </div>' +
              (data.description.length ? ' <div class="suggestion-description">' + data.description + '</div>' : '') +
              '</div>';
        },
        formatSelection: function (data) {
            return data.id;
        },
        minimumInputLength: 0
    });
    $panes.find('input.trigger_branchFilter').on('keyup change', function() {
        var $input = AJS.$(this),
            value = $input.val();
        try {
            new RegExp(value);
            $input.attr("data-aui-notification-field", null).attr("data-aui-notification-error", null);
        } catch (error) {
            $input.attr("data-aui-notification-field", '').attr("data-aui-notification-error", '');
        }
    });
};

TeamCityTrigger.addRepoConfigurationTrigger = function () {
    var $form = AJS.$(TeamCityTrigger.repoConfigurationSelector),
        $tabsContainer = $form.find('.teamcity-triggers'),
        $configCountInput = $form.find('#meta_configCount'),
        tabCount = $tabsContainer.find('> .tabs-menu > .menu-item').length,
        id = tabCount + 1, menuItem, tabsPane;

    if ($form.length === 0) {
         console.error('TeamCity trigger: Cannot add trigger, form not found.');
         return;
    }
    if (id > TeamCityTrigger.maxTriggerCount) {
        console.error('TeamCity trigger: Cannot add trigger, maximum reached');
        return;
    }

    menuItem = nl.empoly.bitbucket.template.hook.menuItem({
        id: id, config: {}, isActive: false
    });
    tabsPane = nl.empoly.bitbucket.template.hook.tabsPane({
        id: id, config: {}, isActive: false
    });

    $tabsContainer.find('.tabs-menu .add-tab').before(menuItem);
    $tabsContainer.append(tabsPane);
    $configCountInput.val(id);

    TeamCityTrigger.initRepoConfigurationTabsPane($form.find('#tab-' + id));
    TeamCityTrigger.updateRepoConfigurationButtons(tabCount + 1);

    AJS.tabs.change('#menu-item-' + id + ' a');
}

TeamCityTrigger.removeRepoConfigurationTrigger = function (id) {
    var $form = AJS.$(TeamCityTrigger.repoConfigurationSelector),
        $tabsContainer = $form.find('.teamcity-triggers'),
        $configCountInput = $form.find('#meta_configCount'),
        tabCount = $tabsContainer.find('> .tabs-menu > .menu-item').length;

    if ($form.length === 0) {
         console.error('TeamCity trigger: Cannot remove trigger, form not found.');
         return;
    }
    if (tabCount === 1 || id > tabCount) {
        console.error('TeamCity trigger: Cannot remove trigger, only 1 tab or id is larger than tabcount');
        return;
    }

    $form.find('#menu-item-' + id).remove();
    $form.find('#tab-' + id).remove();
    $configCountInput.val(tabCount - 1);
    AJS.tabs.change('#menu-item-' + (id < tabCount ? id + 1 : id - 1) + ' a');

    for (var i = id; i < tabCount; i++) {
        var id = i + 1,
            oldIdAttr = '_'+id+'_',
            newIdAttr = '_'+i+'_',
            oldNameAttr = '['+id+']',
            newNameAttr = '['+i+']',
            $menuItem = $form.find('#menu-item-' + id),
            $tab = $form.find('#tab-' + id),
            $nameInput = $tab.find('#trigger_' + id + "_name"),
            $removeButton = $tab.find('#remove-tab-' + id),
            $element,
            name = $nameInput.val();
        if (name.length == 0) {
            name = i;
        }
        $nameInput.attr('placeholder', i);
        $menuItem.attr('id', 'menu-item-' + i).attr('title', name)
            .find('a').attr('href', '#tab-' + i).text(name);
        $tab.attr('id', 'tab-' + i);
        $tab.find('[id*='+oldIdAttr+']').each(function () {
            $element = AJS.$(this),
                id = $element.attr('id'),
                name = $element.attr('name');
            if (id) {
                $element.attr('id', id.replace(oldIdAttr, newIdAttr));
            }
            if (name) {
                $element.attr('name', name.replace(oldNameAttr, newNameAttr));
            }
        });
        $tab.find('[for*='+oldIdAttr+']').each(function() {
            $element = AJS.$(this);
            $element.attr('for', $element.attr('for').replace(oldIdAttr, newIdAttr));
        });
        $removeButton.attr('id', 'remove-tab-' + i)
            .attr('onclick', 'TeamCityTrigger.removeRepoConfigurationTrigger(' + i + ')');
    }

    TeamCityTrigger.updateRepoConfigurationButtons(tabCount - 1);
};

TeamCityTrigger.updateRepoConfigurationButtons = function (tabCount) {
    var $form = AJS.$(TeamCityTrigger.repoConfigurationSelector),
        $firstRemoveButton = $form.find('#remove-tab-1'),
        $menu = $form.find('.tabs-menu'),
        $menuItems = $menu.find('.menu-item'),
        $addButton = $menu.find('.add-tab');
    if (tabCount > 1) {
        $firstRemoveButton.attr('disabled', null).css('display', '');
    } else {
        $firstRemoveButton.attr('disabled', '').css('display', 'none');
    }
    if (tabCount < TeamCityTrigger.maxTriggerCount) {
        $addButton.css('display', '');
    } else {
        $addButton.css('display', 'none');
    }

    $menuItems.css('max-width', Math.min(250, ($menu.width() - 100) / $menuItems.length) + 'px');
};